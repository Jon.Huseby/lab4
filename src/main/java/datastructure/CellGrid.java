package datastructure;

import cellular.CellState;
import java.util.ArrayList;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    private CellState[][] cells;

    public CellGrid(int rows, int columns, CellState initialState) {
		// Do
        this.rows = rows;
        this.cols = columns;
        cells = new CellState[rows][columns];

        for (int row=0; row<rows; row++){
            for (int col=0; col<cols; col++){
                cells[row][col] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // Do 
        return this.rows;
    }

    @Override
    public int numColumns() {
        // Do
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // Do
        if (row >= 0 && row < numRows()) {
            if (column >= 0 && column < numColumns()) {
                cells[row][column] = element;
            } else {
                throw new IndexOutOfBoundsException();
            }
        } else {
            throw new IndexOutOfBoundsException();
        }   
    }

    @Override
    public CellState get(int row, int column) {
        // Do
        if (row >= 0 && row < numRows()) {
            if (column >= 0 && column < numColumns()) {  
                return cells[row][column];
            } else {
                throw new IndexOutOfBoundsException();
            }
        } else {
            throw new IndexOutOfBoundsException();
        } 
    }

    @Override
    public IGrid copy() {
        // Do
        IGrid grid = new CellGrid(this.rows, this.cols, CellState.DEAD);

        for (int row=0; row<rows; row++){
            for (int col=0; col<cols; col++){
                CellState state = this.get(row, col);
                grid.set(row, col, state);
            }
        }
        return grid;
    }
}
